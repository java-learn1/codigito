package com.codigito.nano;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class FindDuplicate {

    public static void main(String args[]) {
        String input = "abcabc";
        int dup = findDuplicatePro(input);
        System.out.println(dup);
    }

    public static int findDuplicate(String input) {
        int indexDuplicate = 0;
        char[] inputChar = input.toCharArray();

        int i=1;
        for (char c: inputChar) {
            indexDuplicate = input.substring(i).indexOf(String.valueOf(c));
            if(indexDuplicate>-1){
                indexDuplicate +=i;
                break;
            }
            i++;
        }
        return indexDuplicate;
    }

    public static int findDuplicatePro(String input){
        Set<Character> seen = new HashSet<>();

        for (int i = 0; i < input.length(); i++) {
            if(seen.contains(input.charAt(i))){
                return i;
            }else{
                seen.add(input.charAt(i));
            }
        }
        return -1;
    }
}


