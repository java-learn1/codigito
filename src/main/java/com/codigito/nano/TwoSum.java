package com.codigito.nano;

import java.util.HashSet;
import java.util.Set;

public class TwoSum {
    public static void main(String args[]){
        //int[] nums= {1, 2, 3, 4};
        int[] nums={1,4,5,1,6};
        int target = 12;
        System.out.println("Result: " + twoSumPro(nums, target));
    }

    public static boolean twoSum(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            int firstTerm = nums[i];
            for (int j = i+1; j < nums.length; j++) {
                int secondTerm = nums[j];
                if (firstTerm+secondTerm == target){
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean twoSumPro(int[] nums, int target) {
        Set<Integer> seen = new HashSet<>();
        for (int num:nums) {
            if(seen.contains(num)){
                return true;
            }
            seen.add(target-num);
        }
        return false;
    }
}
