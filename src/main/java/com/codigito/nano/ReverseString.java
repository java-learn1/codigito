package com.codigito.nano;

public class ReverseString {

    public static void main (String args[]){
        String input = "Hello World!";
        String reverse = reverseStringPro(input);
        System.out.println(reverse);
    }

    public static String reverseString(String s) {
        String reverse = new String();
        for (int i = s.length()-1 ; i >= 0; i--) {
            reverse = reverse.concat(String.valueOf(s.toCharArray()[i]));
        }
        return reverse;
    }

    public static String reverseStringPro(String s) {
        StringBuilder reverse = new StringBuilder();
        for (int i = s.length()-1; i >=0; i--) {
            reverse.append(s.charAt(i));
        }
        return reverse.toString();
    }
}
