package com.codigito.nano;

import java.util.*;
import java.util.stream.Collectors;

public class TopKLargest {
    public static void main (String args[]){

        int [] nums = {5, 10, 22, 100, 8};
        int k = 2;
        //ArrayList<Integer> topK = findTopK(nums, k);

        List<Integer> listTopK = topKLarger(nums,k);

        listTopK.stream().forEach(System.out::println);
    }

    public static ArrayList<Integer> findTopK (int[] nums, int k){
        ArrayList<Integer> numsArray = new ArrayList<>();
        ArrayList<Integer> sortedArrayByK = new ArrayList<>();
        for (int num: nums) {
            numsArray.add(num);
        }
        Collections.sort(numsArray);

        for (int i = nums.length - k; i < nums.length; i++) {
            sortedArrayByK.add(numsArray.get(i));
        }
        return sortedArrayByK;
    }

    public static List<Integer> topKLarger(int[] arr, int k) {
        TreeSet<Integer> topSet = new TreeSet<>();

        for (int num:arr) {
            topSet.add(num);
            if (topSet.size()>k){
                topSet.pollFirst();
            }
        }
        return topSet.stream().collect(Collectors.toList());
    }
}
