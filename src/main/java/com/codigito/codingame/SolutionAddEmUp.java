package com.codigito.codingame;

import java.util.*;

public class SolutionAddEmUp {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        PriorityQueue<Integer> cards = new PriorityQueue<Integer>();
        for (int i = 0; i < N; i++) cards.add(in.nextInt());

        int cost = 0;
        while (cards.size() > 1) {
            int nxtCard = cards.poll() + cards.poll();
            cost += nxtCard;
            cards.add(nxtCard);
        }

        System.out.println(cost);
    }
}
