package com.codigito.codingame;

import java.util.*;
import java.util.stream.Collectors;

public class SolutionPassengers {

    public static void main (String args[]){
        List<String> dataPassengers = Arrays.asList(new String[]{"Luisa Diaz-BUSINESS-1579188410",
                "Juan Romero-ECONOMY-1579188430",
                "Camila Greco-ECONOMY-1579188420",
                "Mariano Camareli-BUSINESS-1579188440"});


        List<String> passengersSort = orderPassengersPro(dataPassengers);

        for (String passenger:passengersSort) {
          System.out.println("Passenger: " + passenger);
        }
    }

    private static List<String> orderPassengersPro(List<String> dataPassengers) {
        if(dataPassengers.size()>0){
            List<String> resultPassengersSort = dataPassengers.stream().filter(s->s.contains("BUSINESS")).collect(Collectors.toList());

            List<String> finalResultPassengersSort = resultPassengersSort;
            dataPassengers.stream().filter(s->!s.contains("BUSINESS")).sorted(new ComparatorByDataString()).forEach(passenger-> finalResultPassengersSort.add(passenger));
            return resultPassengersSort;
        }
        return null;
    }

    private static ArrayList<Passenger> sortPassangers(ArrayList<Passenger> passengers) {
        final String TYPE_BUSINESS = "BUSINESS";
        final String TYPE_ECONOMY = "ECONOMY";

        ArrayList<Passenger> sortPassengers = new ArrayList<>();
        ArrayList<Passenger> economyPassengers = new ArrayList<>();

        for (Passenger passenger:passengers) {
            if(passenger.type.equals(TYPE_BUSINESS)){
                sortPassengers.add(passenger);
            }else if (passenger.type.equals(TYPE_ECONOMY)){
                economyPassengers.add(passenger);
            }
        }

        economyPassengers.sort(new ComparatorByDate());
        for (Passenger passenger:economyPassengers) {
            sortPassengers.add(passenger);
        }
        return sortPassengers;
    }

    private static ArrayList<Passenger> makePassengers(List<String> dataPassengers) {
        final int NAME = 0;
        final int TYPE = 1;
        final int DATE = 2;
        ArrayList<Passenger> passengers = new ArrayList<>();
        for (String data : dataPassengers) {
            String[] dataP = data.split("-");
            String name = dataP[NAME];
            String type = dataP[TYPE];
            String date = dataP[DATE];
            Passenger passenger = new Passenger(name,type,date);
            passengers.add(passenger);
        }

        return passengers;
    }

    static public class Passenger {
        String name;
        String type;
        Date date;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public String toString(){
            return "Passanger: " + this.getName() + ", " +  this.getType() +", " + this.date.toString();
        }
        public Passenger (String name, String type, String date){
            this.name = name;
            this.type = type;
            this.date = new Date(Long.parseLong(date)*1000);
        }
    }

    static public class ComparatorByDate implements Comparator<Passenger>{
        @Override
        public int compare(Passenger passenger1, Passenger passenger2) {
            return passenger1.getDate().compareTo(passenger2.getDate());
        }
    }

    static public class ComparatorByDataString implements Comparator<String>{
        @Override
        public int compare(String passenger1, String passenger2) {
            String datePassenger1 = passenger1.split("-")[2];
            String datePassenger2 = passenger2.split("-")[2];
            return datePassenger1.compareTo(datePassenger2);
        }
    }
}
