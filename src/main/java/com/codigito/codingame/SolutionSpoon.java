package com.codigito.codingame;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SolutionSpoon {
    public static void main(String args[]) {
        //Scanner in = new Scanner(System.in);
        //int width = in.nextInt(); // the number of cells on the X axis
        //int height = in.nextInt(); // the number of cells on the Y axis



//        if (in.hasNextLine()) {
//            in.nextLine();
//        }

//        WIDTH: 5
//        HEIGHT: 1
        //Case 1
        //int width = 2;
        //int height = 2;
        //Case 2
        //int width = 5;
        //int height = 1;
        //Case 3
        int width = 1;
        int height = 4;

//        WIDTH: 1
//        HEIGHT: 4
//        LINE: 0
//        LINE: 0
//        LINE: 0
//        LINE: 0

        System.err.println("WIDTH: "+ width);
        System.err.println("HEIGHT: "+ height);
        List<String> nodosLine = new ArrayList<>();

//        for (int i = 0; i < height; i++) {
//            String line = in.nextLine(); // width characters, each either 0 or .
//            nodosLine.add(line);
//            System.err.println("LINE: " + line);
//        }

//        WIDTH: 5
//        HEIGHT: 1
//        LINE: 0.0.0
        //nodosLine.add("00");
        //nodosLine.add("0.");
        //Case 2
        //nodosLine.add("0.0.0");
        //Case 3
        nodosLine.add("0");
        nodosLine.add("0");
        nodosLine.add("0");
        nodosLine.add("0");

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        List<String> theMatrix = getMatrixPower(nodosLine, width, height);
        theMatrix.forEach(System.out::println);
        // Three coordinates: a node, its right neighbor, its bottom neighbor
//        LINE: 00
//        LINE: 0.
//        System.err.println("0 0 1 0 0 1");
//        System.out.println("1 0 -1 -1 -1 -1");
//        System.out.println("0 1 -1 -1 -1 -1");
    }

    private static List<String> getMatrixPower(List<String> nodosLine, int width, int height) {
        List<String> matrixPower = new ArrayList<>();

        HashMap<String, String> powerNodes = new HashMap<>();
        //Por fila y columna.
        for (int i = 0; i < height; i++) {
            String nodeLine = nodosLine.get(i);
            char[] dataNodeLine = nodeLine.toCharArray();
            for (int j = 0; j <  width; j++) {
                String valueNode = (dataNodeLine[j]=='0') ? String.valueOf(j) + " " + String.valueOf(i): "-1 -1";
                if (dataNodeLine[j]=='0'){
                    powerNodes.put(String.valueOf(i) + String.valueOf(j),valueNode);
                }
            }
        }

        Set<String> keyNodesPower = powerNodes.keySet().stream().sorted().collect(Collectors.toCollection(LinkedHashSet::new));
        for (String keyXY : keyNodesPower) {
            char[] dataKeyXY = keyXY.toCharArray();
            StringBuilder sbPower = new StringBuilder();

            sbPower.append(powerNodes.get(keyXY));

            int x = Integer.parseInt(String.valueOf(dataKeyXY[0]));
            int y = Integer.parseInt(String.valueOf(dataKeyXY[1]));

            int startY = y;
            while(startY<=width){
                String valueNodeRight = powerNodes.get(String.valueOf(x)+String.valueOf(startY+1));
                if (valueNodeRight != null){
                    sbPower.append(" "+valueNodeRight);
                    break;
                }
                if (startY == width){
                    sbPower.append(" -1 -1");
                }
                startY++;
            }

            startY = y;
            while(x<=height) {
                String valueNodeInf = powerNodes.get(String.valueOf(x + 1) + String.valueOf(startY));
                if (valueNodeInf != null) {
                    sbPower.append(" " + valueNodeInf);
                    break;
                }
                if (x == height){
                    sbPower.append(" -1 -1");
                }
                x++;
            }
            matrixPower.add(sbPower.toString());
        }

        return matrixPower;
    }
}
