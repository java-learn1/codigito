package com.codigito.codingame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class SolutionRockPaperScissorsLizardSpock {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();

        List<Player> players = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            int NUMPLAYER = in.nextInt();
            String SIGNPLAYER = in.next();

            players.add(new Player(NUMPLAYER, SIGNPLAYER));
        }

        Player champion = championsLeague(players);

        System.out.println(champion.getNumplayer());
        StringBuilder sb = new StringBuilder();
        champion.getDefeated().forEach(d-> {
            sb.append(d.numplayer + " ");
        });
        String defeats = sb.toString();
        System.out.println(defeats.substring(0, defeats.length()-1));
    }

    private static Player championsLeague(List<Player> players) {

        if (players.size() == 1) {
            return players.get(0);
        }

        List<Player> champions = new ArrayList<>();
        for (int i = 0; i < players.size(); i++) {
            Player champion = fight(players.get(i), players.get(++i));
            champions.add(champion);
        }
        return championsLeague(champions);
    }

    private static Player fight(Player playerOne, Player playerSecond) {
        //Scissors cuts Paper //Scissors decapitates Lizard
        if (playerOne.getSignplayer().equals("C")) {
            if (playerSecond.getSignplayer().equals("P")
                    || playerSecond.getSignplayer().equals("L")) {
                playerOne.setDefeated(playerSecond);
                return playerOne;
            }
        }

        if (playerSecond.getSignplayer().equals("C")) {
            if (playerOne.getSignplayer().equals("P")
                    || playerOne.getSignplayer().equals("L")) {
                playerSecond.setDefeated(playerOne);
                return playerSecond;
            }
        }

        //Paper covers Rock //Paper disproves Spock
        if (playerOne.getSignplayer().equals("P")) {
            if (playerSecond.getSignplayer().equals("R")
                    || playerSecond.getSignplayer().equals("S")) {
                playerOne.setDefeated(playerSecond);
                return playerOne;
            }
        }

        if (playerSecond.getSignplayer().equals("P")) {
            if (playerOne.getSignplayer().equals("R")
                    || playerOne.getSignplayer().equals("S")) {
                playerSecond.setDefeated(playerOne);
                return playerSecond;
            }
        }

        //Rock crushes Lizard //Rock crushes Scissors
        if (playerOne.getSignplayer().equals("R")) {
            if (playerSecond.getSignplayer().equals("L")
                    || playerSecond.getSignplayer().equals("C")) {
                playerOne.setDefeated(playerSecond);
                return playerOne;
            }
        }

        if (playerSecond.getSignplayer().equals("R")) {
            if (playerOne.getSignplayer().equals("L")
                    || playerOne.getSignplayer().equals("C")) {
                playerSecond.setDefeated(playerOne);
                return playerSecond;
            }
        }

        //Lizard poisons Spock //Lizard eats Paper
        if (playerOne.getSignplayer().equals("L")) {
            if (playerSecond.getSignplayer().equals("S")
                    || playerSecond.getSignplayer().equals("P")) {
                playerOne.setDefeated(playerSecond);
                return playerOne;
            }
        }

        if (playerSecond.getSignplayer().equals("L")) {
            if (playerOne.getSignplayer().equals("S")
                    || playerOne.getSignplayer().equals("P")) {
                playerSecond.setDefeated(playerOne);
                return playerSecond;
            }
        }

        //Spock smashes Scissors //Spock vaporizes Rock
        if (playerOne.getSignplayer().equals("S")) {
            if (playerSecond.getSignplayer().equals("C")
                    || playerSecond.getSignplayer().equals("R")) {
                playerOne.setDefeated(playerSecond);
                return playerOne;
            }
        }

        if (playerSecond.getSignplayer().equals("S")) {
            if (playerOne.getSignplayer().equals("C")
                    || playerOne.getSignplayer().equals("R")) {
                playerSecond.setDefeated(playerOne);
                return playerSecond;
            }
        }

        Player champion = null;
        if (playerOne.getSignplayer().equals(playerSecond.getSignplayer())){
            if (playerOne.getNumplayer()>playerSecond.getNumplayer()){
                playerSecond.setDefeated(playerOne);
                champion = playerSecond;
            }
            if (playerOne.getNumplayer()<playerSecond.getNumplayer()){
                playerOne.setDefeated(playerSecond);
                champion = playerOne;
            }
        }
        return  champion;
    }


    static class Player {
        int numplayer;
        String signplayer;
        List<Player> defeated = null;

        public Player(int numplayer, String signplayer) {
            this.numplayer = numplayer;
            this.signplayer = signplayer;
        }

        public int getNumplayer() {
            return numplayer;
        }

        public void setNumplayer(int numplayer) {
            this.numplayer = numplayer;
        }

        public String getSignplayer() {
            return signplayer;
        }

        public void setSignplayer(String signplayer) {
            this.signplayer = signplayer;
        }

        public void setDefeated(Player player) {
            if (this.defeated == null) {
                this.defeated = new ArrayList<>();
            }
            this.defeated.add(player);
        }

        public List<Player> getDefeated() {
            return defeated;
        }
    }
}
