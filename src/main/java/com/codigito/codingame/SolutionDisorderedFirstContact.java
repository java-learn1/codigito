package com.codigito.codingame;

import java.util.*;
import java.io.*;
import java.math.*;

public class SolutionDisorderedFirstContact {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        System.err.println("N: " + N);
        if (in.hasNextLine()) {
            in.nextLine();
        }
        String MESSAGE = in.nextLine();
        System.err.println("mess: " + MESSAGE);
        // Write an answer using System.out.println()
        // To debug: System.err.println("Debug messages...");

        if(N>0){
            while(N>0) {
                MESSAGE = decodeMessage(MESSAGE);
                N-=1;
            }
        }

        if(N<0){
            while(N<0){
                MESSAGE = encodeMessage(MESSAGE);
                N+=1;
            }
        }

        System.out.println(MESSAGE);
    }

    private static List<Integer> getParts (String message){
        ArrayList<Integer> parts = new ArrayList<>();
        for (int i = 0; i < message.length(); i++) {
            int sum = parts.stream().mapToInt(num->num).sum();
            if (parts.contains(message.length()-sum)){
                parts.add(message.length()-sum);
                break;
            }
            parts.add(i+1);
        }
        return parts;
    }

    private static String encodeMessage(String message) {
        String encodeM = "";

        List<Integer> parts = getParts(message);
        int start = 0;
        int end = message.length();
        for (int i = 0; i < parts.size() ; i++) {
            if(i%2==0){
                //start
                encodeM =  encodeM + message.substring(start, start + parts.get(i)) ;
                start= start + parts.get(i);
            } else{
                //end
                encodeM =  message.substring(start, start + parts.get(i)) + encodeM;
                start = start + parts.get(i);
            }
        }
        return encodeM;
    }

    private static String decodeMessage(String message) {
        String decodeM = "";

        List<Integer> parts = getParts(message);
        int start = 0;
        int end = message.length();
        for (int i = parts.size()-1; i >=0 ; i--) {
            if(i%2==0){
                //end
                decodeM =  message.substring(end - parts.get(i), end) + decodeM;
                end = end - parts.get(i);
            } else{
                //start
                decodeM =  message.substring(start, start + parts.get(i)) + decodeM;
                start= parts.get(i);
            }
        }

        return decodeM;
    }
}
