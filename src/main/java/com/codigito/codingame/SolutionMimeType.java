package com.codigito.codingame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class SolutionMimeType {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt(); // Number of elements which make up the association table.
        int Q = in.nextInt(); // Number Q of file names to be analyzed.

        HashMap<String, String> extMimeType = new HashMap<>();
        for (int i = 0; i < N; i++) {
            String EXT = in.next(); // file extension
            String MT = in.next(); // MIME type.
            extMimeType.put(EXT.toLowerCase(), MT);
        }
        List<String> fNames = new ArrayList<>();
        in.nextLine();
        for (int i = 0; i < Q; i++) {
            String FNAME = in.nextLine(); // One file name per line.
            fNames.add(FNAME);
        }

        // Write an answer using System.out.println()
        // To debug: System.err.println("Debug messages...");

        fNames.stream().forEach(fName ->{
            String ext = getExt(fName);
            if (ext != null){
                String mimeType = extMimeType.get(ext);
                if (mimeType != null){
                    System.out.println(mimeType);
                } else{
                    System.out.println("UNKNOWN");
                }
            } else if ( ext == null){
                System.out.println("UNKNOWN");
            }
        });

        // For each of the Q filenames, display on a line the corresponding MIME type. If there is no corresponding type, then display UNKNOWN.
        //System.out.println("UNKNOWN");
    }

    private static String getExt(String fName) {
        String ext = null;
        if(!fName.endsWith(".")){
            String[] dataFName = fName.split("\\.");
            if(dataFName.length>1){
                ext = (dataFName[dataFName.length-1]).toLowerCase();
            }
        }
        return ext;
    }
}
