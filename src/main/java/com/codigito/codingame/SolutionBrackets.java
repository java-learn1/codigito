package com.codigito.codingame;

import java.util.*;
import java.io.*;
import java.math.*;

public class SolutionBrackets {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);

        String expression = in.next();

        // Write an answer using System.out.println()
        // To debug: System.err.println("Debug messages...");
        boolean valideExp = validateExpressionPro(expression);
        System.out.println(valideExp);
    }


    private static boolean validateExpressionPro(String expression){
        System.err.println(expression);
        Stack<Character> makeExpressionValidate = new Stack<>();
        HashMap<Character, Character> openClose = new HashMap<>();
        openClose.put('}', '{');
        openClose.put(']', '[');
        openClose.put(')', '(');

        for (char c : expression.toCharArray()) {
            switch (c){
                case '{':
                case'[':
                case'(':
                    makeExpressionValidate.push(c);
                    break;
                case '}':
                case ']':
                case ')':
                    if (makeExpressionValidate.empty()){
                        return false;
                    }
                    char lastT = makeExpressionValidate.pop();
                    if (openClose.get(c) != lastT){
                        return false;
                    }
                default: continue;
            }
        }

        if (!makeExpressionValidate.empty()){
            return false;
        }
        return true;
    }
}
