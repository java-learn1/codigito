package com.codigito.codingame;

import java.util.Scanner;

public class SolutionThorEp1 {

    static final String N = "N";    //(North)
    static final String NE = "NE";  //(North-East)
    static final String E = "E";    //(East)
    static final String SE = "SE";  //(South-East)
    static final String S = "S";    //(South)
    static final String SW = "SW";  //(South-West)
    static final String W = "W";    //(West)
    static final String NW = "NW";  //(North-West)

    static int startX = 0;
    static int startY = 0;
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int lightX = in.nextInt(); // the X position of the light of power
        int lightY = in.nextInt(); // the Y position of the light of power
        int initialTx = in.nextInt(); // Thor's starting X position
        int initialTy = in.nextInt(); // Thor's starting Y position

        startX = initialTx;
        startY = initialTy;
        // game loop
        while (true) {
            int remainingTurns = in.nextInt(); // The remaining amount of turns Thor can move. Do not remove this line.
            System.err.println(remainingTurns);
            // Write an action using System.out.println()
            // To debug: System.err.println("Debug messages...");
            if (remainingTurns<=0)break;


            String mov = calculateMov(lightX, lightY, startX, startY);

            // A single line providing the move to be made: N NE E SE S SW W or NW
            System.out.println(mov);
        }
    }

    private static String calculateMov(int lightX, int lightY, int initialTx, int initialTy) {
        String mov = "";
        System.err.println("lX: "+lightX);
        System.err.println("lY: "+lightY);
        System.err.println("sX: "+initialTx);
        System.err.println("sY: "+initialTy);

        String movX = "";
        String movY = "";

        //Están en la misma X
        if (initialTx == lightX){
            if (initialTy > lightY){
                movY = N;
                startY -=1;
            }
            if (initialTy < lightY){
                movY = S;
                startY +=1;
            }
        } else{
            if (initialTx > lightX){
                movX = W;
                startX-=1;
            }
            if (initialTx < lightX){
                movX = E;
                startX+=1;
            }
        }

        //Están en la misma Y
        if (initialTy == lightY){
            if (initialTx > lightX){
                movX = W;
                startX-=1;
            }
            if (initialTx < lightX){
                movX = E;
                startX+=1;
            }
        } else{
            if (initialTy > lightY){
                movY = N;
                startY -=1;
            }
            if (initialTy < lightY){
                movY = S;
                startY +=1;
            }
        }

        return mov.concat(movY).concat(movX);
    }

}
