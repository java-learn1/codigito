package com.codigito.codingame;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SolutionBulkEmailGenerator {

    public static void main(String[] args) {
        //Read inputs.
        Scanner scanner = new Scanner(System.in);

        int N = Integer.parseInt(scanner.nextLine());

        String[] emailTemplateLines = new String[N];

        for (int i = 0; i < N; i++) {
            emailTemplateLines[i] = scanner.nextLine();
        }

        String emailTemplate = String.join("\n", emailTemplateLines);

        //Create regex objects.
        Pattern regexPattern = Pattern.compile("\\([^)]*\\)");
        Matcher regexMatcher = regexPattern.matcher(emailTemplate);

        int choicesCounter = -1;
        String email = "";
        int emailTemplateIndex = 0;

        //Replacing choices.
        while (regexMatcher.find()) {
            choicesCounter++;

            email += emailTemplate.substring(emailTemplateIndex, regexMatcher.start());
            emailTemplateIndex = regexMatcher.end();

            String[] choices = emailTemplate.substring(regexMatcher.start() + 1, regexMatcher.end() - 1).split("\\|", -1);
            email += choices[choicesCounter % choices.length];
        }

        email += emailTemplate.substring(emailTemplateIndex);

        //Output email.
        System.out.println(email);
    }

    private static List<String> processEmail(List<String> email) {
        List<String> result = new ArrayList<>();

        List<String> values = new ArrayList<>();
        int i = 0;
        for (String emailProcess : email) {
            if (emailProcess.length() < 0) {
                result.add(emailProcess);
                continue;
            }

            int indexStart = emailProcess.indexOf("(");
            int indexEnd = emailProcess.indexOf(")");
            if (indexStart > -1 && indexEnd > -1) {
                String valueData = emailProcess.substring(indexStart + 1, indexEnd);
                String[] valuesPossible = valueData.split("\\|");

                String emailResult = emailProcess.replace("(" + valueData + ")", valuesPossible[i]);
                result.add(emailResult);
            } else {
                //Suma una linea blanca.
                result.add(emailProcess);
            }
            i += 1;
        }
        return result;
    }
}
