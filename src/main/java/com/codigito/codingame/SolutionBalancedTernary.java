package com.codigito.codingame;

import java.util.Scanner;

public class SolutionBalancedTernary {
    public static void main(String[] args) {
        //Read input.
        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();

        if (N == 0) {
            System.out.println("0");
        } else {
            //Get ternary representation.
            System.out.println(convertBalancedTernary(N));
        }
    }

    private static String convertBalancedTernary(int num) {
        //Case neg
        if (num < 0){
            String numBT = convertBalancedTernary(-num);
            StringBuilder negNumBT = new StringBuilder();

            char[] dataNumBT = numBT.toCharArray();
            for (int i = 0; i < numBT.length(); i++) {
                char c = dataNumBT[i];
                negNumBT.append((c == '1') ? "T" : (c == 'T') ? '1' : '0');
            }
            return negNumBT.toString();
        }

        //Condición de frenada de
        if (num == 0){
            return "";
        }

        if (num%3==2){
            return convertBalancedTernary((num + 1) / 3) + "T";
        } else {
            return convertBalancedTernary(num / 3 ) + (num % 3);
        }

    }
}
