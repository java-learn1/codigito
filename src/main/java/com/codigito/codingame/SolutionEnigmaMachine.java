package com.codigito.codingame;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class SolutionEnigmaMachine {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        String operation = in.nextLine();
        int pseudoRandomNumber = in.nextInt();
        if (in.hasNextLine()) {
            in.nextLine();
        }
        List<String> rotors = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            String rotor = in.nextLine();
            //System.err.println(rotor);
            rotors.add(rotor);
        }
        String message = in.nextLine();

        // Write an answer using System.out.println()
        // To debug: System.err.println("Debug messages...");

        String messageResult = "";
        String abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        if (operation.equals("ENCODE")){
            messageResult = encodeMessage(abc, rotors, pseudoRandomNumber, message);
        } else if (operation.equals("DECODE")){
            messageResult = decodeMessage(abc, rotors, pseudoRandomNumber, message);
        }

        System.out.println(messageResult);
    }

    private static String decodeMessage(String abc, List<String> rotors, int pseudoRandomNumber, String message) {
        String seed = message;
        for (int i = rotors.size()-1 ; i >= 0; i--) {
            char[] dataABC = abc.toCharArray();
            String rotor = (String) rotors.get(i);

            StringBuilder newSeed = new StringBuilder();
            for (char cSeed : seed.toCharArray()) {
                int index = (rotor.indexOf(cSeed));
                newSeed.append(dataABC[index]);
            }

            seed = newSeed.toString();
        }

        message = getMessageOfSeed(abc,pseudoRandomNumber,seed);
        return message;
    }

    private static String encodeMessage(String abc, List<String> rotors, int pseudoRandomNumber, String message) {
        String seed = getSeed(abc,pseudoRandomNumber,message);

        Iterator itRotors = rotors.iterator();
        while(itRotors.hasNext()){
            String rotor = (String) itRotors.next();
            char[] dataRotor = rotor.toCharArray();

            StringBuilder newSeed = new StringBuilder();
            for (char cSeed : seed.toCharArray()) {
                int index = (abc.indexOf(cSeed));
                newSeed.append(dataRotor[index]);
            }

            seed = newSeed.toString();
        }

        return seed;
    }

    private static String getSeed(String abc, int pseudoRandomNumber, String message) {
        StringBuilder seedSB = new StringBuilder();

        char[] dataABC = abc.toCharArray();
        int count = 1;
        for (char c : message.toCharArray()) {
            int index = (abc.indexOf(c)-1) + pseudoRandomNumber + count;
            index = (index< dataABC.length)? index : index%dataABC.length;
            char newC = dataABC[index];
            seedSB.append(newC);
            count+=1;
        }

        return seedSB.toString();
    }

    private static String getMessageOfSeed(String abc, int pseudoRandomNumber, String seed) {
        StringBuilder messageSB = new StringBuilder();

        //THEQUICKBROWNFOXJUMPSOVERALAZYSPHINXOFBLACKQUARTZ

        char[] dataABC = abc.toCharArray();
        int count = 0;
        for (char c : seed.toCharArray()) {
            int index = (abc.indexOf(c)) - pseudoRandomNumber - count;
            index = (index < 0)? dataABC.length - Math.abs(index%dataABC.length)  : index;
            if(index == dataABC.length) index = (abc.indexOf(c));
            char newC = dataABC[index];
            messageSB.append(newC);
            count+=1;
        }

        return messageSB.toString();
    }
}
