package com.codigito.codingame;

public class SolutionPalindrome {

    public static void main (String args[]){
        String str = "CivIC";
        System.out.println(SolutionPalindrome.checkPalindrome("CivIC"));   // true
        System.out.println(SolutionPalindrome.checkPalindrome("Civic")); // true
        System.out.println(SolutionPalindrome.checkPalindrome("Civil"));   // false
        System.out.println(SolutionPalindrome.checkPalindrome("Distance"));     // false
        System.out.println(SolutionPalindrome.checkPalindrome(null));   // true
        System.out.println(checkPalindrome(str));;
    }

    private static boolean checkPalindrome(String str) {

        if (str == null){
            return false;
        }else{
            str = str.replace(" ", "").trim().toUpperCase();
        }
        if (str.length()<2){
            return true;
        } else{
            char data[] = str.toCharArray();
            if(data[0]!=data[data.length-1]){
                return false;
            } else{
                return checkPalindrome(str.substring(1, str.length()-1) );
            }
        }
    }

    static boolean checkPalindromePro(String str) {
        if (str == null){
            return false;
        }
        if(str.length()<2){
            return true;
        } else{
            char data[] = str.toCharArray();
            if(data[0]!=data[data.length-1]){
                return false;
            } else{
                checkPalindrome(str.substring(1,str.length()-1));
            }
        }
        return false;
    }
}
