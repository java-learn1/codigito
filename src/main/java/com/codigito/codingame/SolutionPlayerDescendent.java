package com.codigito.codingame;

import java.util.*;

public class SolutionPlayerDescendent {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int topMountainH = 0; // highest mount 0-9
        int indexMountainDel = 0; // highest mount number 0-7
        // game loop
        while (true) {
            for (int i = 0; i < 8; i++) {
                int mountainH = in.nextInt(); // represents the height of one mountain.
                if (topMountainH <= mountainH) {
                    topMountainH = mountainH;
                    indexMountainDel = i;

                }
            }

            // Write an action using System.out.println()
            // To debug: System.err.println("Debug messages...");
            System.err.println(topMountainH);

            System.out.println(indexMountainDel); // The index of the mountain to fire on.
            topMountainH = 0; //zeroig for the next loop
            indexMountainDel = 0;
        }
    }
    //0 6 7 5 0 8 1 0
}
