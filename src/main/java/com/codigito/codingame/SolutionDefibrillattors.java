package com.codigito.codingame;

import java.util.*;
import java.util.stream.Collectors;

public class SolutionDefibrillattors {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        String LON = in.next();
        String LAT = in.next();
        int N = in.nextInt();
        if (in.hasNextLine()) {
            in.nextLine();
        }

        List<String> defibs = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            String DEFIB = in.nextLine();
            defibs.add(DEFIB);
        }

        // Write an answer using System.out.println()
        // To debug: System.err.println("Debug messages...");

        String dir = getDirMin(defibs, LON, LAT);
        System.out.println(dir);
    }

    private static String getDirMin(List<String> defibs, String lon, String lat) {
        String dir = "";

        HashMap<String, Double> distanceFibs = new HashMap<>();
        for (String dirFib : defibs){
            String[] dataDirFib = dirFib.split(";");

            String lonD = dataDirFib[dataDirFib.length-2];
            String latD = dataDirFib[dataDirFib.length-1];

            double distance = calculateDist (lon, lat, lonD, latD);
            String dataCity = dataDirFib[0].concat(";").concat(dataDirFib[1]);
            distanceFibs.put(dataCity,distance);
        }

        Map<String, Double> sortFibs = distanceFibs.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e2, e1) -> e2, LinkedHashMap::new));

        Map.Entry<String, Double> minEntry = null;
        Double minDistance = Collections.min(sortFibs.values());

        for(Map.Entry<String, Double> entry : sortFibs.entrySet()) {
            Double value = entry.getValue();
            if(null != value && minDistance == value) {
                minEntry = entry;
            }
        }

        String keyCityMin = minEntry.getKey();
        String[] dataCity = keyCityMin.split(";");
        dir = dataCity[1];
        return dir;
    }

    private static double calculateDist(String lon, String lat, String lonD, String latD) {
        double lonA = Double.parseDouble(lon.replace(",","."));
        double latA = Double.parseDouble(lat.replace(",","."));

        double lonB = Double.parseDouble(lonD.replace(",","."));
        double latB = Double.parseDouble(latD.replace(",","."));

        double x = (lonB-lonA)*Math.cos((latA+latB)/2);
        double y = (latB-latA);

        double dist = Math.sqrt((Math.pow(x,2)+Math.pow(y,2)))*6371;
        return dist;
    }
}
