package com.codigito.codingame;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class SolutionOrderOfSuccession {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        List<Person> descendents = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            String name = in.next();
            String parent = in.next();
            int birth = in.nextInt();
            String death = in.next();
            String religion = in.next();
            String gender = in.next();
            Person person = new Person(name, parent, birth, death, religion, gender);
            descendents.add(person);
        }

        // Write an answer using System.out.println()
        // To debug: System.err.println("Debug messages...");

        List<Person> descendentsOoS= orderOfSuccession(descendents);
        descendentsOoS.stream().forEach(d-> System.out.println(d.name));
    }

    private static List<Person> orderOfSuccession(List<Person> descendents) {
        int sizeOfSuccession = (int) descendents.stream().filter(person -> person.death.equals("-")&&person.religion.equals("Anglican")).count();
        List<Person> descendentsOfS = new ArrayList<>();
        Collections.sort(descendents);
        Stack<Person> stackPerson = new Stack<>();

        //root
        stackPerson.push(descendents.remove(0));

        List<Person> rest = new ArrayList<>();
        while (descendentsOfS.size() < sizeOfSuccession){
            Person parent = stackPerson.peek();
            if(parent.death.equals("-") && parent.religion.equals("Anglican")){
                descendentsOfS.add(parent);
            }else{
                descendents.remove(parent);
            }

            if (descendents.size()>0){
                List<Person> hisSonParent = getSonParent(parent, descendents);
                if (hisSonParent != null){
                    Person hisDescendent = getHisDescendent(hisSonParent);

                    if (hisDescendent != null){
                        stackPerson.push(hisDescendent);
                        hisSonParent.remove(hisDescendent);
                        descendents.remove(hisDescendent);
                    }
                    rest.addAll(hisSonParent);
                } else {
                    Person brother = getBrother(parent, rest);

                    if (brother != null){
                        stackPerson.push(brother);
                        //remove.
                        descendents.remove(brother);
                        rest.remove(brother);
                    }else {

                        //Eliminar al Padre y sus hijos del stack.
                        String parentName = parent.parent;
                        Predicate<Person> predicateRemove = person -> person.parent.equals(parentName) || person.name.equals(parentName);
                        stackPerson.removeIf(predicateRemove);

                        Person lastParent = stackPerson.peek();
                        Optional<Person> personOptional = rest.stream().filter(person -> person.parent.equals(lastParent.name) && person.gender.equals("M")).sorted().findFirst();

                        if(personOptional.isPresent()){
                            stackPerson.push(personOptional.get());
                            rest.remove(personOptional.get());
                            descendents.remove(personOptional.get());
                        }else{

                            Optional<Person> restPersonOptional = rest.stream().filter(person -> person.parent.equals(lastParent.name) && person.death.equals("-")).sorted().findFirst();

                            if(restPersonOptional.isPresent()){
                                stackPerson.push(restPersonOptional.get());
                                rest.remove(restPersonOptional.get());
                                descendents.remove(restPersonOptional.get());
                            }else{

                                Optional<Person> rest2PersonOptional = rest.stream().filter(person -> person.death.equals("-") && person.gender.equals("M")).sorted().findFirst();
                                if(rest2PersonOptional.isPresent()) {
                                    stackPerson.push(rest2PersonOptional.get());
                                    rest.remove(rest2PersonOptional.get());
                                    descendents.remove(rest2PersonOptional.get());
                                }else{
                                    //Si son hermanos tomar el mas viejo.
                                    String parentRest = rest.get(0).parent;
                                    int countBrothers = rest.size();

                                    int countRestBrothers = (int) rest.stream().filter(person -> person.parent.equals(parentRest)).count();
                                    if(countBrothers == countRestBrothers){
                                        stackPerson.push(rest.get(0));
                                        //remove
                                        rest.remove(0);
                                        descendents.remove(stackPerson.peek());
                                    }else {

                                        stackPerson.push(rest.get(rest.size() - 1));
                                        //remove
                                        rest.remove(rest.size() - 1);
                                        descendents.remove(stackPerson.peek());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return descendentsOfS;
    }

    private static Person getBrother(Person brotherRoot, List<Person> rest) {
        Optional<Person> brother = null;
        brother = rest.stream().filter(person -> person.parent.equals(brotherRoot.parent)).findFirst();
        return (brother.isPresent())? brother.get() : null;
    }

    private static List<Person> getSonParent(Person parent, List<Person> descendents) {
        List<Person> sonsParent = new ArrayList<>();

        //Get Sons M
        sonsParent.addAll(descendents.stream().
                    filter(person -> person.parent.equals(parent.name))
                    .collect(Collectors.toList()));

        return (sonsParent.size()>0)? sonsParent : null;
    }

    private static Person getHisDescendent(List<Person> persons) {
        Person hisDescendent = null;
        for (Person p : persons) {
            if (p.gender.equals("M")){
                hisDescendent = p;
                return hisDescendent;
            }
        }

        for (Person p: persons){
            hisDescendent = p;
            return hisDescendent;
        }
        return hisDescendent;
    }


    public static class Person implements Comparable<Person> {
        String name;
        String parent;
        int birth;
        String death;
        String religion;
        String gender;

        public Person(String name, String parent, int birth, String death, String religion, String gender) {
            this.name = name;
            this.parent = parent;
            this.birth = birth;
            this.death = death;
            this.religion = religion;
            this.gender = gender;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getParent() {
            return parent;
        }

        public void setParent(String parent) {
            this.parent = parent;
        }

        public int getBirth() {
            return birth;
        }

        public void setBirth(int birth) {
            this.birth = birth;
        }

        public String getDeath() {
            return death;
        }

        public void setDeath(String death) {
            this.death = death;
        }

        public String getReligion() {
            return religion;
        }

        public void setReligion(String religion) {
            this.religion = religion;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        @Override
        public int compareTo(Person person) {
            return (this.birth - person.birth);
        }
    }
}
