package com.codigito.codingame;

import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class SolutionTemperature {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt(); // the number of temperatures to analyse

        final int ZERO = 0;
        int resultTemperature = 0;
        for (int i = 0; i < n; i++) {
            int t = in.nextInt(); // a temperature expressed as an integer ranging from -273 to 5526

            if (t == ZERO){
                resultTemperature = ZERO;
                in.close();
                break;
            }else{
                if (i==0){
                    resultTemperature = t;
                } else {
                    if (Math.abs(t) - Math.abs(resultTemperature) == ZERO){
                        if (t>resultTemperature){
                            resultTemperature = t;
                        }
                    } else if (Math.abs(t) - Math.abs(resultTemperature) < ZERO){
                        resultTemperature = t;
                    }
                }
            }
        }
        in.close();

        // Write an answer using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(resultTemperature);
    }
}
