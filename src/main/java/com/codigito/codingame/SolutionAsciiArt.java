package com.codigito.codingame;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class SolutionAsciiArt {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int L = in.nextInt();
        int H = in.nextInt();
        if (in.hasNextLine()) {
            in.nextLine();
        }
        String T = in.nextLine();
        System.err.println(T);

        HashMap<Integer, List<String>> abc = new HashMap<>();
        for (int i = 0; i < H; i++) {
            String ROW = in.nextLine();
            System.err.println(ROW);
            abc = processROW(abc, ROW, L);
        }

        List<String> dataABC = new ArrayList<>(Arrays.asList("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"));
        HashMap<String, Integer> mapIndexABC = new HashMap<>();
        for (int i = 0; i < dataABC.size(); i++) {
            mapIndexABC.put(dataABC.get(i),i);
        }

        printT(T, abc, mapIndexABC, L, H);


        // Write an answer using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println("answer");
    }

    private static void printT(String texto, HashMap<Integer, List<String>> abc, HashMap<String, Integer> mapIndexABC, int L, int H) {
        //asciiArt.stream().forEach(s->System.out.println(s));
        char[] dataChartText = texto.toCharArray();
        for (int i = 0; i < H; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < texto.length(); j++) {
                char c = dataChartText[j];
                if (c!= ' '){
                    Integer indexChar = mapIndexABC.get(String.valueOf(c).toUpperCase());
                    indexChar = (indexChar!=null) ? indexChar : 26;
                    List<String> asciiArt = abc.get(indexChar);
                    sb.append(asciiArt.get(i));
                } else {
                    sb.append(" ");
                }
            }
            System.out.println(sb.toString());
        }
    }

    private static HashMap<Integer, List<String>> processROW(HashMap<Integer, List<String>> abc, String row, int l) {

        int i = 0;
        int j = 0;
        List<String> asciiArt;
        while(i<= row.length()-1){
            String data = row.substring(i, i+l);

            if (abc.get(j) == null){
               asciiArt = new ArrayList<>();
            } else {
                asciiArt = abc.get(j);
            }
            asciiArt.add(data);
            abc.put(j,asciiArt);

            i = i+l;
            j +=1;
        }
        return abc;
    }


}
