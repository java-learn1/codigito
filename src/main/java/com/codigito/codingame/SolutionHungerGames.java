package com.codigito.codingame;

import java.util.*;
import java.util.stream.Collectors;

public class SolutionHungerGames {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int tributes = in.nextInt();
        if (in.hasNextLine()) {
            in.nextLine();
        }

        Map<String, TreeSet<String>> dataKillers = new TreeMap<>();
        HashMap<String, String> dataKilledsByKiller = new HashMap<>();
        for (int i = 0; i < tributes; i++) {
            String playerName = in.nextLine();
            dataKillers.put(playerName, new TreeSet<>());
        }

        int turns = in.nextInt();
        if (in.hasNextLine()) {
            in.nextLine();
        }
        List<String> infoGame = new ArrayList<>();
        for (int i = 0; i < turns; i++) {
            String info = in.nextLine();

            int index = info.indexOf("killed");
            String killer = info.substring(0, index - 1);
            String rest = info.substring(index + "killed".length() + 1);

            Arrays.stream(rest.split(","))
                    .map(String::trim)
                    .forEach(tribute -> {
                        dataKillers.get(killer).add(tribute);
                        dataKilledsByKiller.put(tribute, killer);
                    });

            infoGame.add(info);
        }

        int i = 0;
        for (Map.Entry<String, TreeSet<String>> entry : dataKillers.entrySet()) {
            String name = entry.getKey();
            String killer, killed;

            killed = entry.getValue().isEmpty()
                    ? "None"
                    : entry.getValue().stream().collect(Collectors.joining(", "));

            killer = dataKilledsByKiller.getOrDefault(name, "Winner");

            System.out.println("Name: " + name);
            System.out.println("Killed: " + killed);
            System.out.println("Killer: " + killer);

            if (i != dataKillers.size() - 1) {
                System.out.println();
            }
            i++;
        }
    }
}
