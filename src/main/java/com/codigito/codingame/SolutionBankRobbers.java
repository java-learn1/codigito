package com.codigito.codingame;

import java.util.Arrays;
import java.util.Scanner;

public class SolutionBankRobbers {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int R = in.nextInt();
        System.err.println("Ladrones: "+ R );
        int V = in.nextInt();
        System.err.println("Bovedas: "+ V );
        int[] T = new int[R];
        for (int i = 0; i < V; i++) {
            int C = in.nextInt();
            System.err.println("C: "+ C );
            int N = in.nextInt();
            T[0] += (int)(Math.pow(10, N) * Math.pow(5, C - N));
            Arrays.sort(T);
        }

        //Print time of the robber with the most time.
        System.out.println(T[R - 1]);
    }
}
