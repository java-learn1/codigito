package com.codigito.codingame;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class SolutionHorseRacing {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();

        List<Integer> piHorses = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            int pi = in.nextInt();
            piHorses.add(pi);
        }

        int diffPi = getDifference(piHorses);

        // Write an answer using System.out.println()
        // To debug: System.err.println("Debug messages...");
        System.out.println(diffPi);
    }

    private static int getDifference(List<Integer> piHorses) {
        int diffMin = Integer.MAX_VALUE;
        piHorses = piHorses.stream().sorted().collect(Collectors.toList());

        for (int i = 0; i < piHorses.size()-1; i++) {
            diffMin = Math.min(diffMin, piHorses.get(i+1)- piHorses.get(i));
        }
        return diffMin;
    }

}
