package com.codigito.codingame;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SolutionAneoSponsored {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int speed = in.nextInt();
        int lightCount = in.nextInt();

        List<Integer[]> dataLights = new ArrayList<>();
        for (int i = 0; i < lightCount; i++) {
            int distance = in.nextInt();
            int duration = in.nextInt();

            Integer[] data = {distance, duration};
            dataLights.add(data);
        }

        double speedMaxMS = convertSpeedKMH(speed);

        double speedRun = testSpeedAneo(dataLights, speedMaxMS);
        int speedMaxRun = (int) convertSpeedMS(speedRun);
        // Write an answer using System.out.println()
        // To debug: System.err.println("Debug messages...");
        System.out.println(speedMaxRun);
    }

    private static double testSpeedAneo(List<Integer[]> dataLights, double speedMaxMS) {
        int allDistance = 0;
        int allTime = 0;

        allDistance = dataLights.stream().mapToInt(dataLight->dataLight[0]).sum();
        allTime = dataLights.stream().mapToInt(dataLight->dataLight[1]).sum();

        double speedMaxRec = 0.0;
        double speedN = ((double) allDistance)/((double)allTime);

        if (speedN < speedMaxMS){
            return speedMaxMS;
        }

        int count = 1;
        while (speedN > speedMaxMS){
            count+=1;
            speedN = speedN/2;
        }

        speedMaxRec = speedN;


        return speedMaxRec;
    }

    private static double convertSpeedKMH(int speed) {
        double speedMS = Double.parseDouble(String.valueOf(speed)) * 1000 /3600;
        return speedMS;
    }

    private static double convertSpeedMS(double speed) {
        double speedMS = speed * 3600 /1000;
        return speedMS;
    }
}
