package com.codigito.codingame;

import java.util.Scanner;

public class SolutionBatmanShadws {
    static final String UP = "U";  //(Up)
    static final String UR = "UR"; //(Up-Right)
    static final String R = "R";   //(Right)
    static final String DR = "DR"; //(Down-Right)
    static final String D = "D";   //(Down)
    static final String DL ="DL";  //(Down-Left)
    static final String L = "L";   //(Left)
    static final String UL = "UL"; //(Up-Left)

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int W = in.nextInt(); // width of the building.
        int H = in.nextInt(); // height of the building.
        int N = in.nextInt(); // maximum number of turns before game over.
        int X0 = in.nextInt();
        int Y0 = in.nextInt();

        System.err.println("W: " + W);
        System.err.println("H: " + H);
        System.err.println("N: " + N);
        System.err.println("X0: " + X0);
        System.err.println("Y0: " + Y0);

//        Standard Error Stream:
//        W: 4
//        H: 8
//        N: 40
//        X0: 2
//        Y0: 3
//        Bomb dir:DR
//        Standard Output Stream:
//        2 3
//        Game information:
//        Batman moved from window (2,3) to window (2,3)
//        The bombs are located below and to the right of Batman

        // game loop
        int startW = 0;
        int endW = W;
        int startH = 0;
        int endH = H;
        while (true) {
            String bombDir = in.next(); // the direction of the bombs from batman's current location (U, UR, R, DR, D, DL, L or UL)
            System.err.println("Bomb dir:" + bombDir);

            if (N == 0){
                break;
            }else {
                switch (bombDir){
                    case UP:
                        endH = Y0;
                        Y0 = binaryY(bombDir, startH, endH, Y0);
                        break;
                    case UR:
                        startW =X0;
                        X0 = binaryX(R, startW, endW, X0);
                        endH = Y0;
                        Y0 = binaryY(UP, startH, endH, Y0);
                        break;
                    case R:
                        startW = X0;
                        X0 = binaryX(bombDir, startW, endW, X0);
                        break;
                    case DR:
                        startW = X0;
                        X0 = binaryX(R, startW, endW, X0);
                        startH = Y0;
                        Y0 = binaryY(D, startH, endH, Y0);
                        break;
                    case D:
                        startH = Y0;
                        Y0 = binaryY(bombDir, startH, endH, Y0);
                        break;
                    case DL:
                        endW = X0;
                        X0 = binaryX(L, startW, endW, X0);
                        startH = Y0;
                        Y0 = binaryY(D, startH, endH, Y0);
                        break;
                    case L:
                        endW = X0;
                        X0 = binaryX(bombDir, startW, endW, X0);
                        break;
                    case UL:
                        endW = X0;
                        X0 = binaryX(L, startW, endW, X0);
                        endH = Y0;
                        Y0 = binaryY(UP, startH, endH, Y0);
                        break;
                }

            }

            // Write an action using System.out.println()
            // To debug: System.err.println("Debug messages...");
            // the location of the next window Batman should jump to.
            N-=1;
            System.out.println(String.valueOf(X0).concat(" ").concat(String.valueOf(Y0)));
        }
    }

    static int calculateX (String bombDir, int W, int X0){
        switch (bombDir){
            case R:
                X0 += W;
                break;
            case L:
                X0 -= W;
                break;
        }

        System.err.println("CALCULATE X0:" + X0);
        if (X0 < 0){
            X0 = 0;
            System.err.println("X0 0:" + X0);
        } else if (W<X0){
            X0 -= 1;
            System.err.println("X0 W:" + X0);
        }

        return X0;
    }

    static int calculateY (String bombDir, int H, int Y0){
        int oldYO = Y0;
        switch (bombDir){
            case D:
                Y0 += H;
                break;
            case UP:
                Y0 -= H;
                break;
        }

        if(Y0<0){
            Y0+=1;
        }else if (H<Y0){
            Y0-=1;
        }
//        System.err.println("CALCULATE Y0:" + Y0);
//        if (Y0<0 || H<Y0 ){
//            bombDir = (bombDir.equals(D))? UP: D;
//            //calculateY(bombDir, H/2, Y0);
//        }
        return Y0;
    }


    static int binaryX (String bombDir, int startW, int endW, int X0){
        switch (bombDir){
            case R:
                X0 = (startW + endW)/2;
                break;
            case L:
                X0 = (startW + endW)/2;
                break;
        }
        return X0;
    }

    static int binaryY (String bombDir, int startH, int endH, int Y0){
        switch (bombDir){
            case D:
                Y0 = (startH + endH)/2;
                break;
            case UP:
                Y0 = (startH + endH)/2;
                break;
        }
        return Y0;
    }
}
