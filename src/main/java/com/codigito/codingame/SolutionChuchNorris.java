package com.codigito.codingame;

import java.util.Formatter;
import java.util.Scanner;

public class SolutionChuchNorris {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        String MESSAGE = in.nextLine();

        // Write an answer using System.out.println()
        // To debug: System.err.println("Debug messages...");

        String messageChucCode = getChucCode(MESSAGE);

        System.out.println(messageChucCode);
    }

    private static String getChucCode(String message) {
        final String codeChucZero = "00";
        final String codeChucOne = "0";

        String binaryMessage = getBinaryMessage(message);

        char[] dataBinaryMessage = binaryMessage.toCharArray();
        char currentChar = dataBinaryMessage[0];
        int current = 0;
        StringBuilder sbChuc = new StringBuilder();
        for (char binaryChar : dataBinaryMessage) {
            if (binaryChar != currentChar){
                if (currentChar == '0'){
                    sbChuc.append(codeChucZero);
                }

                if (currentChar == '1'){
                    sbChuc.append(codeChucOne);
                }

                sbChuc.append(" ");
                for (int j = 0; j < current; j++) {
                    sbChuc.append("0");
                }
                sbChuc.append(" ");
                currentChar = binaryChar;
                current =1;
            } else {
                current +=1;
            }
        }

        if (currentChar == '0'){
            sbChuc.append(codeChucZero);
        }

        if (currentChar == '1'){
            sbChuc.append(codeChucOne);
        }

        sbChuc.append(" ");
        if (current > 1){
            for (int j = 0; j < current; j++) {
                sbChuc.append("0");
            }
        }

        return sbChuc.toString();
    }


    private static String getBinaryMessage (String message){
        StringBuilder sb = new StringBuilder();
        for(char c : message.toCharArray()){
            int ascii = (int) c;
            String binaryMessage = Integer.toBinaryString(ascii);
            if (binaryMessage.length()<8){
                binaryMessage = String.format("%08d", Integer.valueOf(binaryMessage));
            }
            sb.append(binaryMessage);
        }

        return sb.toString();
    }
}
