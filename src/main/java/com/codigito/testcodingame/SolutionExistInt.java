package com.codigito.testcodingame;

import java.util.Arrays;

public class SolutionExistInt {

    public static void main(String args[]) {
        int[] ints = {-9, 14, 37, 102};
        System.out.println(exists(ints, 37)); // true
    }

    private static boolean exists(int[] ints, int k) {
        if (ints.length>0) {
            int size = ints.length;
            int middle = size / 2;

            if (ints[middle] == k)
                return true;
            else if (size == 1)
                return false;
            else if (ints[middle] > k)
                return exists(Arrays.copyOfRange(ints, 0, middle), k);
            else
                return exists(Arrays.copyOfRange(ints, middle + 1, size), k);
        }
        return false;
    }


}
