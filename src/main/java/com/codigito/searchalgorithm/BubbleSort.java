package com.codigito.searchalgorithm;

/**
 * La ordenación de burbujas se utiliza para ordenar
 * una lista de elementos, comparando dos elementos adyacentes
 * e intercambiándolos, si no están en orden.
 * @author guso008
 */
public class BubbleSort {

    static int[] dataSort;

    public void bubbleSort(int[] data){
        boolean swapped = true;
        for (int i = 0; i < data.length -1; i++) {
            swapped = false;

            for (int j = 0; j < data.length - i -1; j++) {
                if(data[j] > data[j+1]){
                    int aux = data[j];
                    data[j] = data[j+1];
                    data[j+1] = aux;
                    swapped = true;
                }
            }
            if (swapped == false)
                break;
        }

        dataSort = data;
    }

    public void printArray() {
        for (int i = 0; i < dataSort.length; i++) {
            System.out.println(dataSort[i] + " ");
        }
    }
}
