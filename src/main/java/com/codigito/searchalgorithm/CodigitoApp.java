package com.codigito.searchalgorithm;

public class CodigitoApp {
    public static void main (String[] args){
        BubbleSort bubble = new BubbleSort();

        int[] data = {5,2,42,6,1,3,2};
        bubble.bubbleSort(data);
        bubble.printArray();
    }

}
