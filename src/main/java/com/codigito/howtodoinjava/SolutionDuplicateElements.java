package com.codigito.howtodoinjava;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SolutionDuplicateElements {

    public static void main (String args[]){

        int[] arr = {3,6,5,7,7,8,19,19,32};

        Set<Integer> magic = IntStream.of(arr).boxed().collect(Collectors.toList()).stream().filter(num->Collections.frequency(IntStream.of(arr).boxed().collect(Collectors.toList()), num)>1).collect(Collectors.toSet());

        System.out.println(magic.toString());
    }
}
