package com.codigito.howtodoinjava.demo;

import java.io.Serializable;
import java.time.LocalDate;

public class Employee implements Serializable, Comparable<Employee> {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String firstName;
    private String lastName;
    private Department department;
    private LocalDate localDate;
    private Integer age;

    public Employee(Long id, String fName, String lName, Department department) {
        super();
        this.id = id;
        this.firstName = fName;
        this.lastName = lName;
        this.department = department;
    }

    public Employee(Long id, String fName, String lName, Integer age) {
        super();
        this.id = id;
        this.firstName = fName;
        this.lastName = lName;
        this.age = age;
    }

    public Employee(Long id, String fName, LocalDate localDate) {
        super();
        this.id = id;
        this.firstName = fName;
        this.localDate = localDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Employee [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + "]";
    }

    @Override
    public int compareTo(Employee o) {
        return (int) (this.id - o.id);
    }
}
