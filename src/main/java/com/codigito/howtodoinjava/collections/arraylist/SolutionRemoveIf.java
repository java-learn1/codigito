package com.codigito.howtodoinjava.collections.arraylist;

import com.codigito.howtodoinjava.demo.Employee;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Predicate;

public class SolutionRemoveIf {
    public static void main(String[] args) throws CloneNotSupportedException {
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9,10));

        numbers.removeIf( number -> number%2 == 0 );
        System.out.println(numbers);

        //Example Employee
        ArrayList<Employee> employees = new ArrayList<>();

        employees.add(new Employee(1l, "Alex", LocalDate.of(2018, Month.APRIL, 21)));
        employees.add(new Employee(4l, "Brian", LocalDate.of(2018, Month.APRIL, 22)));
        employees.add(new Employee(3l, "Piyush", LocalDate.of(2018, Month.APRIL, 25)));
        employees.add(new Employee(5l, "Charles", LocalDate.of(2018, Month.APRIL, 23)));
        employees.add(new Employee(2l, "Pawan", LocalDate.of(2018, Month.APRIL, 24)));

        Predicate<Employee> predicate = employee -> employee.getFirstName().startsWith("P");
        employees.removeIf(predicate);

        System.out.println(employees);

    }
}
