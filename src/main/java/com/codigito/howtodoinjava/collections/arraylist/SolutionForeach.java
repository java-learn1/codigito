package com.codigito.howtodoinjava.collections.arraylist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Consumer;

public class SolutionForeach {
    public static void main(String[] args){
        ArrayList<String> names = new ArrayList<>(Arrays.asList("A","B","C","D"));
        //1
        names.forEach(name -> System.out.println(name.toLowerCase()));

        //2
        Consumer<String> lambdaExp = x -> System.out.println(x.toLowerCase());
        names.forEach(lambdaExp);

        //3
        names.forEach(name -> printString(name));

        //4
        names.forEach(SolutionForeach::printString);
    }

    private static void printString(String name) {
        System.out.println(name.toLowerCase());
    }

}
