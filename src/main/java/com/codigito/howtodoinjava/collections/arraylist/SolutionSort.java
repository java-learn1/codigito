package com.codigito.howtodoinjava.collections.arraylist;

import com.codigito.howtodoinjava.demo.Employee;
import com.codigito.howtodoinjava.demo.NameSorter;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Comparator;

public class SolutionSort {
    public static void main(String[] args) throws CloneNotSupportedException {
        ArrayList<Employee> employees = new ArrayList<>();

        employees.add(new Employee(1l, "Alex", LocalDate.of(2018, Month.APRIL, 21)));
        employees.add(new Employee(4l, "Brian", LocalDate.of(2018, Month.APRIL, 22)));
        employees.add(new Employee(3l, "David", LocalDate.of(2018, Month.APRIL, 25)));
        employees.add(new Employee(5l, "Charles", LocalDate.of(2018, Month.APRIL, 23)));
        employees.add(new Employee(2l, "Edwin", LocalDate.of(2018, Month.APRIL, 24)));

        employees.sort(new NameSorter().reversed());
        System.out.println(employees);
    }
}
