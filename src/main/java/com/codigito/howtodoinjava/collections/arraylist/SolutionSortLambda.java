package com.codigito.howtodoinjava.collections.arraylist;

import com.codigito.howtodoinjava.demo.Employee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SolutionSortLambda {

    public static void main (String[] args){
        List<Employee> employees  = getEmployees();

        //Sort all employees by first name
        employees.sort(Comparator.comparing(employee -> employee.getFirstName()));
        //OR you can use below
        employees.sort(Comparator.comparing(Employee::getFirstName));
        System.out.println(employees);
        System.out.println("============REVERSE==================");
        //Sort all employees by first name; And then reversed
        Comparator<Employee> comparator = Comparator.comparing(e-> e.getFirstName());
        employees.sort(comparator.reversed());

        //Let's print the sorted list
        employees.forEach(System.out::println);

        System.out.println("============GROUP BY==================");
        Comparator<Employee> groupByComparator = Comparator.comparing(Employee::getFirstName).thenComparing(Employee::getLastName);
        employees.sort(groupByComparator);

        employees.forEach(System.out::println);

        System.out.println("============PARALLEL SORT==================");
        //Parallel Sorting
        Employee[] employeesArray = employees.toArray(new Employee[employees.size()]);

        Arrays.parallelSort(employeesArray, groupByComparator);
        Arrays.stream(employeesArray).forEach(System.out::println);
    }

    private static List<Employee> getEmployees(){
        List<Employee> employees  = new ArrayList<>();
        employees.add(new Employee(6l,"Yash", "Chopra", 25));
        employees.add(new Employee(2l,"Aman", "Sharma", 28));
        employees.add(new Employee(3l,"Aakash", "Yaadav", 52));
        employees.add(new Employee(5l,"David", "Kameron", 19));
        employees.add(new Employee(4l,"James", "Hedge", 72));
        employees.add(new Employee(8l,"Balaji", "Subbu", 88));
        employees.add(new Employee(7l,"Karan", "Johar", 59));
        employees.add(new Employee(1l,"Lokesh", "Gupta", 32));
        employees.add(new Employee(9l,"Vishu", "Bissi", 33));
        employees.add(new Employee(10l,"Lokesh", "Ramachandran", 60));
        return employees;
    }
}
