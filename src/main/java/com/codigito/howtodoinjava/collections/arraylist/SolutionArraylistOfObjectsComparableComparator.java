package com.codigito.howtodoinjava.collections.arraylist;

import com.codigito.howtodoinjava.demo.AgeSorter;
import com.codigito.howtodoinjava.demo.Employee;
import com.codigito.howtodoinjava.demo.NameSorter;

import java.util.*;

public class SolutionArraylistOfObjectsComparableComparator {
    public static void main(String[] args) {
        Employee e1 = new Employee(1l, "aTestName", "dLastName", 34);
        Employee e2 = new Employee(2l, "nTestName", "pLastName", 30);
        Employee e3 = new Employee(3l, "kTestName", "sLastName", 31);
        Employee e4 = new Employee(4l, "dTestName", "zLastName", 25);

        List<Employee> employees = new ArrayList<Employee>();
        employees.add(e2);
        employees.add(e3);
        employees.add(e1);
        employees.add(e4);

        // UnSorted List
        System.out.println("================UNSORTED======================");
        employees.forEach(System.out::println);

        Collections.sort(employees);
        // Default Sorting by employee id
        System.out.println("================SORTED======================");
        employees.forEach(System.out::println);


        System.out.println("================SORTED NAME=================");
        Collections.sort(employees, new NameSorter());
        // Sorted by firstName
        employees.forEach(System.out::println);

        System.out.println("================SORTED AGE==================");
        Collections.sort(employees, new AgeSorter());

        // Sorted by age
        employees.forEach(System.out::println);

        //Comparador con Lambda en JAVA8
        //Sort all employees by first name
        employees.sort(Comparator.comparing(e -> e.getFirstName()));

        //OR you can use below
        employees.sort(Comparator.comparing(Employee::getFirstName));

        //Sort all employees by first name in reverse order
        Comparator<Employee> comparator = Comparator.comparing(e -> e.getFirstName());
        employees.sort(comparator.reversed());

        //Sorting on multiple fields; Group by.
        Comparator<Employee> groupByComparator = Comparator.comparing(Employee::getFirstName).thenComparing(Employee::getLastName);
        employees.sort(groupByComparator);

        System.out.println("================SORTED groupByComparator==================");
        // groupByComparator
        employees.forEach(System.out::println);

        System.out.println("================SORTED SET==================");
        SortedSet<Employee> set = new TreeSet<Employee>();

        Employee e1Set = new Employee(1l, "aTestName", "dLastName", 34);
        Employee e2Set = new Employee(2l, "nTestName", "pLastName", 30);
        Employee e3Set = new Employee(3l, "kTestName", "sLastName", 31);
        Employee e4Set = new Employee(4l, "dTestName", "zLastName", 25);

        set.add(e2Set);
        set.add(e3Set);
        set.add(e1Set);
        set.add(e4Set);

        set.forEach(System.out::println);

        System.out.println("================SORTED SET NAMESORTED==================");
        SortedSet<Employee> setFirstName = new TreeSet<Employee>(new NameSorter());

        Employee e1FN = new Employee(1l, "aTestName", "dLastName", 34);
        Employee e2FN = new Employee(2l, "nTestName", "pLastName", 30);
        Employee e3FN = new Employee(3l, "kTestName", "sLastName", 31);
        Employee e4FN = new Employee(4l, "dTestName", "zLastName", 25);

        setFirstName.add(e2FN);
        setFirstName.add(e3FN);
        setFirstName.add(e1FN);
        setFirstName.add(e4FN);

        setFirstName.forEach(System.out::println);
    }
}
