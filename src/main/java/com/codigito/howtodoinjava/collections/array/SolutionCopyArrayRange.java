package com.codigito.howtodoinjava.collections.array;

import org.w3c.dom.ls.LSOutput;

import java.util.Arrays;
import java.util.List;

public class SolutionCopyArrayRange {

    public static void main (String args[]){

        String[] names = {"Alex", "Brian", "Charles", "David"};

        //Subarray from index '0' (inclusive) to index '2' (exclusive)
        String[] partialsName = Arrays.copyOfRange(names, 0, 2);
        // [Alex, Brian]
        Arrays.stream(partialsName).forEach(System.out::println);

        //Copy all names from with index '2'
        String[] endNames = Arrays.copyOfRange(names, 2, names.length); // [Charles, David, null, null, null, null, null, null]

        //Copy last 8 names start with index '2'
        //No ArrayIndexOutOfBoundsException error
        String[] moreNames = Arrays.copyOfRange(names, 2, 10);

        List<String> listPartialsName = Arrays.asList( Arrays.copyOfRange(names, 2 , names.length));
        // [Charles, David]
        System.out.println(listPartialsName);

    }
}
