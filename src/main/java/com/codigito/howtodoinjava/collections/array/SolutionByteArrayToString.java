package com.codigito.howtodoinjava.collections.array;

import java.nio.charset.Charset;
import java.util.Base64;

public class SolutionByteArrayToString {
    public static void main(String[] args){
        String name = "howtodoinjava.com";

        byte[] byteArray = name.getBytes();

        String str = new String(byteArray);
        String strWithCharset = new String(byteArray, Charset.defaultCharset());

        System.out.println("Original String: "+ name );
        System.out.println("Obtained String: "+ str );
        System.out.println("Obtained String: "+ strWithCharset );

        byte[] bytes = "hello world".getBytes();

        //Convert byte[] to String
        String string64 = Base64.getEncoder().encodeToString(bytes);
        System.out.println(string64);

        String stringDecode = new String(Base64.getDecoder().decode(string64.getBytes()));
        System.out.println(stringDecode);
    }
}
