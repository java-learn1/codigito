package com.codigito.howtodoinjava.collections.array;

import com.codigito.howtodoinjava.demo.Department;
import com.codigito.howtodoinjava.demo.Employee;

import org.apache.commons.lang3.SerializationUtils;

public class SolutionArrayCloneExample {
    public static void main(String[] args){
        //Clone an array
        Employee[] empArr = new Employee[2];    //Original array

        empArr[0] = new Employee(100l, "Lokesh", "Gupta", new Department(1, "HR"));
        empArr[1] = new Employee(200l, "Pankaj", "Kumar", new Department(2, "Finance"));

        Employee[] clonedArray = empArr.clone();  //Shallow copied array

        empArr[0].setFirstName("Unknown");
        empArr[0].getDepartment().setName("Unknown");

        //Verify the change in original array - "CHANGED"
        System.out.println(empArr[0].getFirstName());                     //Unknown
        System.out.println(empArr[0].getDepartment().getName());          //Unknown

        //Verify the change in cloned array - "CHANGED"
        System.out.println(clonedArray[0].getFirstName());                  //Unknown
        System.out.println(clonedArray[0].getDepartment().getName());       //Unknown

        empArr[0] = new Employee(100l, "Lokesh", "Gupta", new Department(1, "HR"));
        empArr[1] = new Employee(200l, "Pankaj", "Kumar", new Department(2, "Finance"));

        //Array Deep Copy
        Employee[] copiedArray = (Employee[]) SerializationUtils.clone(empArr); //Deep copied array

        empArr[0].setFirstName("Unknown");
        empArr[0].getDepartment().setName("Unknown");

        //Verify the change in original array - "CHANGED"
        System.out.println(empArr[0].getFirstName());                     //Unknown
        System.out.println(empArr[0].getDepartment().getName());          //Unknown

        //Verify the change in deep copied array - "UNCHANGED"
        System.out.println(copiedArray[0].getFirstName());                  //Lokesh
        System.out.println(copiedArray[0].getDepartment().getName());       //HR
    }
}
