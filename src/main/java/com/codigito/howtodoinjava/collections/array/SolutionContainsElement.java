package com.codigito.howtodoinjava.collections.array;

import java.util.ArrayList;
import java.util.Arrays;

public class SolutionContainsElement {

    public static void main (String args[]){

        //Check arraylist contains element
        ArrayList<String> list = new ArrayList<>(Arrays.asList("banana", "guava", "apple", "cheeku"));

        System.out.println(list.contains("apple"));     //true
        System.out.println(list.indexOf("apple"));      //2

        System.out.println(list.contains("lion"));      //false
        System.out.println(list.indexOf("lion"));       //-1

        //Check array contains element
        String[] fruits = new String[]{"banana", "guava", "apple", "cheeku"};

        Arrays.asList(fruits).contains("apple"); // true
        Arrays.asList(fruits).indexOf("apple"); // 2

        Arrays.asList(fruits).contains("lion"); // false
        Arrays.asList(fruits).indexOf("lion"); // -1

        //Check array contains element
        boolean result = Arrays.asList(fruits)
                                .stream()
                                .anyMatch(x->x.equalsIgnoreCase("apple"));

        System.out.println(result);

        boolean result2 = Arrays.asList(fruits)
                .stream()
                .anyMatch(x -> x.equalsIgnoreCase("lion"));  //false

        System.out.println(result2);
    }
}
