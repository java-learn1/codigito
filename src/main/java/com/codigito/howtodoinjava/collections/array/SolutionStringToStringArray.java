package com.codigito.howtodoinjava.collections.array;

import java.util.regex.Pattern;

public class SolutionStringToStringArray {

    public static void main (String args[]){
        //1. String to String[]
        String blogName = "how to do in java";
        String[] words = null;

        // Method 1 :: using String.split() method
        words = blogName.split(" ");

        // Method 2 :: using Pattern.split() method
        Pattern pattern = Pattern.compile(" ");
        String[] wordsPattern = pattern.split(blogName);

        //2. String[] to String
        String blogNameString = String.join(" ", words);
        System.out.println(blogNameString);

    }
}
