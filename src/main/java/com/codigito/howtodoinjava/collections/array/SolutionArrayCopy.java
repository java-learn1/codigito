package com.codigito.howtodoinjava.collections.array;

import java.util.Arrays;

public class SolutionArrayCopy {
    public static void main(String[] args){
        String[] names = {"Alex", "Brian", "Charles", "David"};

        // Use arr.clone() method - Recommended
        String[] namesClone = names.clone();

        // Use Arrays.copyOf() method - Most readable
        String[] namesCopy = Arrays.copyOfRange(names, 0, names.length);

        //Using System.arraycopy() method - Equally efficient but less readable
        String[] namesArrayCopy = new String[names.length];
        System.arraycopy(names,0, namesArrayCopy, 0, names.length);

        System.out.println(Arrays.toString(namesClone));
        System.out.println(Arrays.toString(namesCopy));
        System.out.println(Arrays.toString(namesArrayCopy));
    }
}
