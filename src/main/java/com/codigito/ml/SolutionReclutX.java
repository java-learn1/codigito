package com.codigito.ml;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class SolutionReclutX {

    static final String chainDNAX_AAAA = "AAAA";
    static final String chainDNAX_CCCC = "CCCC";
    static final String chainDNAX_GGGG = "GGGG";
    static final String chainDNAX_TTTT = "TTTT";

    public static void main (String args[]){
        String[] dna = {"ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"};
        boolean isMutant = isMutant(dna);

        System.out.println("he human is Mutant? " + isMutant);
    }

    private static boolean isMutant(String[] dna) {
        boolean isMutant = false;

        long allChainX = 0;
        List<String> horizontalLines = getHorizontalLines(dna);
        allChainX += horizontalLines.stream().filter(isChainX()).count();

        List<String> verticalLines = getVerticalLines(dna);
        allChainX += verticalLines.stream().filter(isChainX()).count();

        List<String> diagonalLines = getDiagonalLines(dna);
        allChainX += diagonalLines.stream().filter(isChainX()).count();

        if(allChainX > 0L){
            isMutant = true;
        }

        return isMutant;
    }

    private static List<String> getHorizontalLines(String[] dna){
        return Arrays.asList(dna);
    }

    private static List<String> getVerticalLines(String[] dna){
        List<String> verticalLines = new ArrayList<>(dna.length);
        for (int i = 0; i < dna.length; i++) {
            StringBuilder verticalLine = new StringBuilder();
            for (String chainDNA : dna) {
                verticalLine.append(chainDNA.charAt(i));
            }
            verticalLines.add(verticalLine.toString());
        }
        return  verticalLines;
    }

    private static List<String> getDiagonalLines(String[] dna){
        List<String> diagonalLines = new ArrayList<>();

        StringBuilder diagonalFirst = new StringBuilder();
        StringBuilder diagonalSecond = new StringBuilder();

        for (int i = 0; i < dna.length; i++) {
            diagonalFirst.append(dna[i].charAt(i));
            diagonalSecond.append(dna[i].charAt((dna.length-1)-i));
        }
        diagonalLines.add(diagonalFirst.toString());
        diagonalLines.add(diagonalSecond.toString());
        return diagonalLines;
    }

    public static Predicate<String> isChainX(){
        return chainDNA -> (chainDNA.indexOf(chainDNAX_AAAA)>-1) ||
                (chainDNA.indexOf(chainDNAX_CCCC)>-1) ||
                (chainDNA.indexOf(chainDNAX_GGGG)>-1) ||
                (chainDNA.indexOf(chainDNAX_TTTT)>-1) ;
    }
}
