package com.codigito.test;

import java.util.*;

public class TestJava {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        String expression = in.next();

        // Write an answer using System.out.println()
        // To debug: System.err.println("Debug messages...");

        boolean resultExp = validateExpression(expression);
        System.out.println(resultExp);
    }

    static boolean validateExpression(String expression){
        boolean result = false;

        final char CURLY_BRACKET_OPEN = '{';
        final char CURLY_BRACKET_CLOSE = '}';
        final char SQUARE_BRACKET_OPEN = '[';
        final char SQUARE_BRACKET_CLOSE = ']';
        final char PARENTHESES_OPEN = '(';
        final char PARENTHESES_CLOSE = ')';

        HashMap<Character, Character> termExp = new HashMap();
        termExp.put(CURLY_BRACKET_CLOSE, CURLY_BRACKET_OPEN);
        termExp.put(SQUARE_BRACKET_CLOSE, SQUARE_BRACKET_OPEN);
        termExp.put(PARENTHESES_CLOSE, PARENTHESES_OPEN);

        Stack<Character> stackExp = new Stack<>();

        for (char charExp : expression.toCharArray()) {
            switch(charExp){
                case CURLY_BRACKET_OPEN:
                case SQUARE_BRACKET_OPEN:
                case PARENTHESES_OPEN:
                    stackExp.push(charExp);
                    break;
                case CURLY_BRACKET_CLOSE:
                case SQUARE_BRACKET_CLOSE:
                case PARENTHESES_CLOSE:
                    //{[{iHTSc}]}p(R)m(){q({})
                    if(stackExp.size()<=0){
                        return false;
                    }
                    if(stackExp.pop() != termExp.get(charExp)){
                        return false;
                    }
                    break;
                default:
                    continue;
            }
        }

        if(stackExp.size()>0){
            return false;
        }
        return result;
    }
}
